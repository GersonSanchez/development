/**
 * @NApiVersion 2.0
 * @NModuleScope Public
 */

/* = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =\
||   This script for customer center (Time)                     ||
||                                                              ||
||  File Name: LMRY_MX_ST_Sales_Wht_Transaction_LBRY_V2.0.js    ||
||                                                              ||
||  Version Date         Author        Remarks                  ||
||  2.0     Jun 01 2021  LatamReady    Use Script 2.0           ||
 \= = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = */

define([
    'N/log',
    'N/record',
    'N/search',
    'N/runtime',
    './LMRY_libSendingEmailsLBRY_V2.0',
    './LMRY_Log_LBRY_V2.0'
], function (log, record, search, runtime, Library_Mail, Library_Log) {
    
    var LMRY_SCRIPT = 'LMRY - MX ST Sales Wht Transaction V2.0';
    var LMRY_SCRIPT_NAME = 'LMRY_MX_ST_Sales_Wht_Transaction_LBRY_V2.0.js';



});